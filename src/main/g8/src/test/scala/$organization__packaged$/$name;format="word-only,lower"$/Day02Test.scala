package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day02Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day02.part1(getInput("day02/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day02.part1(getInput("day02/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day02.part2(getInput("day02/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day02.part2(getInput("day02/input.txt")), -1)
  // }
