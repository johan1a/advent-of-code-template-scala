package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day20Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day20.part1(getInput("day20/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day20.part1(getInput("day20/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day20.part2(getInput("day20/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day20.part2(getInput("day20/input.txt")), -1)
  // }
