package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day06Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day06.part1(getInput("day06/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day06.part1(getInput("day06/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day06.part2(getInput("day06/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day06.part2(getInput("day06/input.txt")), -1)
  // }
