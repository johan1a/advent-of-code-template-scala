package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day24Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day24.part1(getInput("day24/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day24.part1(getInput("day24/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day24.part2(getInput("day24/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day24.part2(getInput("day24/input.txt")), -1)
  // }
