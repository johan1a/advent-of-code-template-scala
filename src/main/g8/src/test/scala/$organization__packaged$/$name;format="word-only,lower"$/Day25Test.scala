package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day25Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day25.part1(getInput("day25/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day25.part1(getInput("day25/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day25.part2(getInput("day25/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day25.part2(getInput("day25/input.txt")), -1)
  // }
