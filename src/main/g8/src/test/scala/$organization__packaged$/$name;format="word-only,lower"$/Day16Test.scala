package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day16Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day16.part1(getInput("day16/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day16.part1(getInput("day16/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day16.part2(getInput("day16/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day16.part2(getInput("day16/input.txt")), -1)
  // }
