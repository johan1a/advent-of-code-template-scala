package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day13Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day13.part1(getInput("day13/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day13.part1(getInput("day13/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day13.part2(getInput("day13/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day13.part2(getInput("day13/input.txt")), -1)
  // }
