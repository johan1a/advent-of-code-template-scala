package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day09Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day09.part1(getInput("day09/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day09.part1(getInput("day09/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day09.part2(getInput("day09/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day09.part2(getInput("day09/input.txt")), -1)
  // }
