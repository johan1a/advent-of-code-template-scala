package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day15Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day15.part1(getInput("day15/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day15.part1(getInput("day15/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day15.part2(getInput("day15/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day15.part2(getInput("day15/input.txt")), -1)
  // }
