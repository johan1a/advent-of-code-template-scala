package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day18Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day18.part1(getInput("day18/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day18.part1(getInput("day18/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day18.part2(getInput("day18/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day18.part2(getInput("day18/input.txt")), -1)
  // }
