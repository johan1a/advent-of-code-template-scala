package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day03Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day03.part1(getInput("day03/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day03.part1(getInput("day03/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day03.part2(getInput("day03/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day03.part2(getInput("day03/input.txt")), -1)
  // }
