package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day04Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day04.part1(getInput("day04/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day04.part1(getInput("day04/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day04.part2(getInput("day04/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day04.part2(getInput("day04/input.txt")), -1)
  // }
