package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day23Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day23.part1(getInput("day23/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day23.part1(getInput("day23/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day23.part2(getInput("day23/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day23.part2(getInput("day23/input.txt")), -1)
  // }
