package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day05Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day05.part1(getInput("day05/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day05.part1(getInput("day05/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day05.part2(getInput("day05/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day05.part2(getInput("day05/input.txt")), -1)
  // }
