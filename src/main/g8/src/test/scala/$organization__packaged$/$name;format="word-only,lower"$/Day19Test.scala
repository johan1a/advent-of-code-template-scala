package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day19Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day19.part1(getInput("day19/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day19.part1(getInput("day19/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day19.part2(getInput("day19/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day19.part2(getInput("day19/input.txt")), -1)
  // }
