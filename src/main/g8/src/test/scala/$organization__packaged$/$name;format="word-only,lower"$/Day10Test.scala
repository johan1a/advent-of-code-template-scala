package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day10Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day10.part1(getInput("day10/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day10.part1(getInput("day10/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day10.part2(getInput("day10/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day10.part2(getInput("day10/input.txt")), -1)
  // }
