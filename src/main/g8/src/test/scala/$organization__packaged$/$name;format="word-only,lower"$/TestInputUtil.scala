package $organization$.$name;format="word-only,lower"$

import scala.io.Source

object TestInputUtil:

  def getInput(path: String): Seq[String] =
    Source.fromResource(path).getLines.toSeq
