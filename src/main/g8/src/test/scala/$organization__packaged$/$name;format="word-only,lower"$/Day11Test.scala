package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day11Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day11.part1(getInput("day11/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day11.part1(getInput("day11/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day11.part2(getInput("day11/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day11.part2(getInput("day11/input.txt")), -1)
  // }
