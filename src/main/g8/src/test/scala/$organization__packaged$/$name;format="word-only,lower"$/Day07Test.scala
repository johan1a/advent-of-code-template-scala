package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day07Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day07.part1(getInput("day07/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day07.part1(getInput("day07/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day07.part2(getInput("day07/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day07.part2(getInput("day07/input.txt")), -1)
  // }
