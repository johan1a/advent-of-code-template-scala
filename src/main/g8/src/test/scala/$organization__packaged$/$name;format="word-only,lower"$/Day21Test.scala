package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day21Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day21.part1(getInput("day21/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day21.part1(getInput("day21/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day21.part2(getInput("day21/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day21.part2(getInput("day21/input.txt")), -1)
  // }
