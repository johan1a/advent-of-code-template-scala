package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day12Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day12.part1(getInput("day12/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day12.part1(getInput("day12/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day12.part2(getInput("day12/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day12.part2(getInput("day12/input.txt")), -1)
  // }
