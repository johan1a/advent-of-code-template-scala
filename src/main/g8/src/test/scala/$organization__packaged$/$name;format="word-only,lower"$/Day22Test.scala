package $organization$.$name;format="word-only,lower"$

import $organization$.$name;format="word-only,lower"$.TestInputUtil.getInput

class Day22Test extends munit.FunSuite:

  test("Part 1 test") {
    assertEquals(Day22.part1(getInput("day22/test.txt")), -1)
  }

  // test("Part 1") {
  //   assertEquals(Day22.part1(getInput("day22/input.txt")), -1)
  // }

  // test("Part 2 test") {
  //   assertEquals(Day22.part2(getInput("day22/test.txt")), -1)
  // }

  // test("Part 2") {
  //   assertEquals(Day22.part2(getInput("day22/input.txt")), -1)
  // }
