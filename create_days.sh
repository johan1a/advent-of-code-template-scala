#!/usr/bin/fish

echo Creating days 2-25 based on Day01...

set day01Path (realpath src/main/g8/src/main/scala/**/Day01.scala)
set day01TestPath (realpath src/main/g8/src/test/scala/**/Day01Test.scala)

for i in (seq 2 25)
  if [ $i -lt 10 ] ;
    set i "0$i"
  end

  set path (echo  $day01Path | sed "s/01/$i/")
  cp -f $day01Path $path
  sed -i "s/01/$i/g" $path

  set testPath (echo  $day01TestPath | sed "s/01/$i/")
  cp -f $day01TestPath $testPath
  sed -i "s/01/$i/g" $testPath

  touch "src/main/g8/src/test/resources/day$i/input.txt"
  touch "src/main/g8/src/test/resources/day$i/test.txt"

end

echo Done.
