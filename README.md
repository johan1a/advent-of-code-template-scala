# advent-of-code-template-scala

A [Giter8][g8] template for advent of code solutions using scala.

## Usage

```
sbt new file://advent-of-code-template-scala.g8
```


## Generate days

Generate days 2-25 based on day01:
(requires fish to be installed)
```
./create_days.sh
```
